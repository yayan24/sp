package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/scrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

const (
	PORT        = ":8080"
	CERT_PERM   = "./src/sp/cert.perm"
	PRIVATE_KEY = "./src/sp/private.key"

	DB_CONN = "mongodb://brayan:brayan123@ds055855.mlab.com:55855/clinicaldb"

	PAGE_INDEX     = "./src/sp/index.html"
	PAGE_LOGIN_ERR = "./src/sp/err_login.html"
	PAGE_SEARCH    = "./src/sp/search.html"
)

type Oid struct {
	Oid string `json:"$oid"`
}

type Name struct {
	First string `json:"first"`
	Last  string `json:"last"`
}

type Common struct {
	Allergies []string `json:"allergies"`
	Blood     string   `json:"blood"`
}

type Patient struct {
	Oid     `json:"_id"`
	Name    `json:"name"`
	Birth   string  `json:"birth"`
	Sip     string  `json:"sip"`
	Dates   []Dates `json:"dates"`
	Common  `json:"common"`
	General []General `json:"general"`
	Radio   []Radio   `json:"radio"`
	Cardio  []Cardio  `json:"cardio"`
}

type Dates struct {
	Id         string `json:"_id"`
	Specialist string `json:"specialist"`
	Date       string `json:"date"`
	Reason     string `json:"reason"`
}

type General struct {
	Id          string   `json:"_id"`
	Description string   `json:"description"`
	Drugs       []string `json:"drugs"`
}

type Radio struct {
	Id        string   `json:"_id"`
	Rx        []string `json:"rx"`
	Diagnosis string   `json:"diagnosis"`
	Date      string   `json:"date"`
}

type Cardio struct {
	Id          string `json:"_id"`
	Description string `json:"description"`
	Diagnosis   string `json:"diagnosis"`
	Date        string `json:"date"`
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Salt     string `json:"salt"`
}

type Privileges struct {
	Username string   `json:"username"`
	Role     []string `json:"role"`
}

var adminFields = []string{"_id", "name", "birth", "sip", "dates"}
var basicFields = []string{"common"}
var generalFields = []string{"general"}
var radioFields = []string{"radio"}
var cardioFields = []string{"cardio"}

var fieldsMap = map[string][]string{
	"admin":   adminFields,
	"basic":   basicFields,
	"general": generalFields,
	"radio":   radioFields,
	"cardio":  cardioFields,
}

var sessions map[string]string = make(map[string]string)

// Función para comprobar errores (ahorra escritura)
func check(e error) {
	if e != nil {
		panic(e)
	}
}

/**

CONTROL DE ACCESO

*/

func index(res http.ResponseWriter, req *http.Request) {
	cookie, _ := req.Cookie("sessionID")

	if cookie != nil && sessions[cookie.Value] != "" {
		log.Println("Session detected. Redirecting...")
		http.Redirect(res, req, "/search", http.StatusSeeOther)
	} else {
		page, err := ioutil.ReadFile(PAGE_INDEX)
		check(err)

		fmt.Fprintf(res, string(page))
	}
}

func login(res http.ResponseWriter, req *http.Request) {
	cookie, _ := req.Cookie("sessionID")

	if cookie != nil && sessions[cookie.Value] != "" {
		log.Println("Session detected. Redirecting...")
		http.Redirect(res, req, "/search", http.StatusSeeOther)
	} else {
		username := req.PostFormValue("username")
		password := req.PostFormValue("password")

		if checkUser(username, password) {
			log.Println("Login successful!")

			sessionIdLength := 10
			sessionId := make([]byte, sessionIdLength)
			rand.Read(sessionId)
			sessionIdStr := base64.StdEncoding.EncodeToString(sessionId)

			sessions[sessionIdStr] = username

			cookie := http.Cookie{Name: "sessionID", Value: sessionIdStr, HttpOnly: false}

			http.SetCookie(res, &cookie)

			log.Println("A cookie for the session has been created")

			http.Redirect(res, req, "/search", http.StatusSeeOther)
		} else {
			log.Println("Invalid login combination!")
			page, err := ioutil.ReadFile(PAGE_LOGIN_ERR)
			check(err)
			fmt.Fprintf(res, string(page))
		}
	}
}

func checkUser(username string, password string) bool {
	matched, _ := regexp.MatchString("[a-z]{5}", username)
	if !matched {
		return false
	}

	matched, _ = regexp.MatchString(".{8}", password)
	if !matched {
		return false
	}

	session, err := mgo.Dial(DB_CONN)
	check(err)
	defer session.Close()

	log.Println("Connected to users DB!")
	session.SetMode(mgo.Monotonic, true)
	collection := session.DB("clinicaldb").C("users")

	user := User{}
	err = collection.Find(bson.M{"username": username}).One(&user)

	if err != nil || user.Password != encryptPasswd(password, user.Salt) {
		return false
	} else {
		return true
	}
}

/**

GESTIÓN DE HISTORIAS

*/

func search(res http.ResponseWriter, req *http.Request) {
	cookie, _ := req.Cookie("sessionID")

	if cookie == nil || sessions[cookie.Value] == "" {
		log.Println("Invalid access to search form detected. Redirecting...")
		http.Redirect(res, req, "/index", http.StatusSeeOther)
	} else {
		page, err := ioutil.ReadFile(PAGE_SEARCH)
		check(err)

		fmt.Fprintf(res, string(page))
	}
}

func selectFields(q ...string) (r bson.M) {
	r = make(bson.M, len(q))

	for _, s := range q {
		r[s] = 1
	}

	return
}

func getRole(req *http.Request) []string {
	session, err := mgo.Dial("mongodb://test:test@ds055855.mlab.com:55855/clinicaldb")
	if err != nil {
		log.Fatalf("Error: ", err)
	}

	log.Println("Connected to roles DB!")

	session.SetMode(mgo.Monotonic, true)
	collection := session.DB("clinicaldb").C("roles")

	cookie, _ := req.Cookie("sessionID")
	priv := Privileges{}
	err = collection.Find(bson.M{"username": sessions[cookie.Value]}).One(&priv)

	if err != nil {
		log.Println("An error happened while retrieving a role...")
	} else {
		log.Println("Role retrieved succesfully!")
	}

	return priv.Role
}

func getPatient(sip string, fields []string) (Patient, bool) {
	session, err := mgo.Dial("mongodb://test:test@ds055855.mlab.com:55855/clinicaldb")
	if err != nil {
		log.Fatalf("Error: ", err)
	}

	log.Println("Connected to patients DB!")

	session.SetMode(mgo.Monotonic, true)
	collection := session.DB("clinicaldb").C("patients")

	retrieved := true
	patient := Patient{}
	err = collection.Find(bson.M{"sip": sip}).Select(selectFields(fields...)).One(&patient)

	if err != nil {
		log.Println("An error happened while retrieving a patient...")
		retrieved = false
	} else {
		log.Println("Patient retrieved succesfully!")
	}

	return patient, retrieved
}

func getData(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")

	log.Println("Responding to sip request")
	log.Println(req.UserAgent())

	vars := mux.Vars(req)
	sip := vars["sip"]

	res.WriteHeader(http.StatusOK)

	// Comprobar cookie, obtener usuario y comprobar permisos
	role := getRole(req)
	fields := getFields(role)

	patient, retrieved := getPatient(sip, fields)

	if retrieved {
		patientPrintable, _ := json.Marshal(patient)
		fmt.Fprintf(res, string(patientPrintable))
	} else {
		fmt.Fprintf(res, "Patient not found!")
	}
}

func getFields(role []string) []string {
	var fields = []string{}
	var tam = len(role)

	for i := 0; i < tam; i++ {
		fields = append(fields, fieldsMap[role[i]]...)
	}

	return fields
}

func encryptPasswd(s string, st string) string {
	password := []byte(s)
	salt, err := base64.StdEncoding.DecodeString(st)

	if err != nil {
		panic(err)
	}

	hashedPassword, err := scrypt.Key(password, salt, 16384, 8, 1, 32)

	if err != nil {
		panic(err)
	}

	return base64.StdEncoding.EncodeToString(hashedPassword)
}

func main() {
	router := mux.NewRouter() //.StrictSlash(true)
	router.HandleFunc("/index", index)
	router.HandleFunc("/login", login)
	router.HandleFunc("/search", search)
	router.HandleFunc("/request/{sip}", getData).Methods("GET")
	router.HandleFunc("/", index)
	http.ListenAndServeTLS(PORT, CERT_PERM, PRIVATE_KEY, router)
}
